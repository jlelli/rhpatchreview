#!/bin/bash

mr_baseurl="https://gitlab.com/redhat/rhel/src/kernel/"

is_gitlab_notification=false

while read line
do
  if [ $(echo "${line}" | grep -c "^X-GitLab-") -gt 0 ]; then
    is_gitlab_notification=true
    if [ $(echo "${line}" | grep -c "^X-GitLab-Project-Path:") -gt 0 ]; then
      gl_proj_path=$(echo "${line}" | cut -d" " -f2)
    elif [ $(echo "${line}" | grep -c "^X-GitLab-MergeRequest-IID:") -gt 0 ]; then
      gl_mr_id=$(echo "${line}" | cut -d" " -f2)
    fi
  fi
  if [ "${is_gitlab_notification}" == "true" ]; then
    continue
  fi
  if [ $(echo "${line}" | grep -c "^${mr_baseurl}") -gt 0 ]; then
    mr_url="${line}"
    break
  elif [ $(echo "${line}" | grep -c "^Merge Request: ${mr_baseurl}") -gt 0 ]; then
    mr_url=$(echo "${line}" | cut -d" " -f3)
    break
  elif [ $(echo "${line}" | grep -c "> Merge Request: ${mr_baseurl}") -gt 0 ]; then
    mr_url="https:$(echo "${line}" | cut -d":" -f3)"
    break
  elif [ $(echo "${line}" | grep -c "> ${mr_baseurl}") -gt 0 ]; then
    mr_url="https:$(echo "${line}" | cut -d" " -f2)"
    break
  fi
done < "${1:-/dev/stdin}"

if [ "${is_gitlab_notification}" == "true" ]; then
  mr_url="https://gitlab.com/${gl_proj_path}/-/merge_requests/${gl_mr_id}"
fi

if [ -z "${mr_url}" ]; then
  echo "No Merge Request URL found in this email!"
  exit 1
fi
if [ $(echo "${mr_url}" | grep -c "merge_requests") -eq 0 ]; then
  echo "This email contains a gitlab URL for something other than an MR."
  exit 1
fi

# Strip any anchors off the MR url too, present in some review comments
mr_url=$(echo "${mr_url}" | cut -d"#" -f1)

/usr/local/bin/rhpatchreview -m ${mr_url}
