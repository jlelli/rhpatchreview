#!/usr/bin/python3
# rhpatchparser - Red Hat Patch Parsing Library
#
# This is a patch parsing and metadata extraction function library,
# shared by pre-submission patch checking tools and maintainer tools.
#
# Copyright (C) 2012-2021 Red Hat Inc.
# Author: Jarod Wilson <jarod@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.
#
# Most of these functions are duplicated from Red Hat's kernel-webhooks, or
# were the original source of functions now found here:
# https://gitlab.com/cki-project/kernel-webhooks/

import difflib
import enum
import os
import sys
import re
import datetime
import tempfile
import time

# requires GitPython
from git import *

class Match(enum.IntEnum):
    """Match versus upstream commit's diff."""

    NOUCID = 0
    FULL = 1
    PARTIAL = 2
    DIFFS = 3
    KABI = 4
    RHELONLY = 5
    POSTED = 6
    BADMAIL = 7
    MERGECOMMIT = 8


def find_bz_in_line(line, prefix):
    """Return bug number from properly formated Bugzilla: line."""
    # BZs must be called out one-per-line, begin with 'Bugzilla:' and contain a complete BZ URL.
    # Plus an exception for INTERNALs.
    pattern = prefix + r': http(s)?://bugzilla\.redhat\.com/(show_bug\.cgi\?id=)?(?P<bug>\d{4,8})$'
    bznum_re = re.compile(pattern)
    bugs = bznum_re.match(line)
    if bugs:
        return bugs.group('bug')
    if line == prefix + ': INTERNAL':
        return 'INTERNAL'
    return None


def extract_all_bzs(message, mr_bugs, dependencies):
    """Extract all BZs from the message."""
    mlines = message.splitlines()
    bzs = []
    non_mr_bzs = []
    dep_bzs = []

    for line in mlines:
        bug = find_bz_in_line(line, 'Bugzilla')
        if not bug:
            continue
        if bug in dependencies:
            dep_bzs.append(bug)
        elif mr_bugs and bug not in mr_bugs:
            non_mr_bzs.append(bug)
        else:
            bzs.append(bug)
    # We return empty arrays if no bugs are found
    return (bzs, non_mr_bzs, dep_bzs)


def extract_bzs(message):
    """Extract BZs from the message."""
    bzs, _x, _y = extract_all_bzs(message, [], [])
    return bzs


cve_re = re.compile('CVE-\d{4,4}-\d{4,4}')
def find_cve_numbers(subject, comments):
    cve_list = []
    subject_cves = cve_re.findall(subject)
    comment_cves = cve_re.findall(comments)
    found_cves = subject_cves + comment_cves
    for cve in found_cves:
        if cve not in cve_list:
            cve_list.append(cve)
    return cve_list


def compare_commits(adiff, bdiff):
    """Perform interdiff of two patches' diffs."""
    interdiff = difflib.unified_diff(adiff.split('\n'), bdiff.split('\n'),
                                     fromfile='diff_a', tofile='diff_b', lineterm="")
    interesting = filter_diff(interdiff)
    return interesting


def compare_commit_to_upstream_filtered(orig_upstream_diff, submission, excludes):
    upstream = tempfile.NamedTemporaryFile(mode = "w")
    filtered = tempfile.NamedTemporaryFile(mode = "r+")

    upstream.write("%s\n" % orig_upstream_diff)
    upstream.flush()

    os.system("filterdiff -p1 -X %s %s > %s" % (excludes, upstream.name, filtered.name))
    filtered.flush()

    temp = filtered.readlines()
    filtered_upstream_diff = ""
    for line in temp:
        filtered_upstream_diff += "%s" % line
    interdiff = difflib.unified_diff(filtered_upstream_diff.split('\n'), submission.split('\n'),    fromfile='upstream', tofile='submission', lineterm="")
    interesting = filter_diff(interdiff)

    return interesting


def extract_ucid_greedy(message):
    """Extract upstream commit ID from the message."""
    message_lines = message.split('\n')
    gitref_list = []
    # Pattern for 'git show <commit>' (and git log) based backports
    gitlog_greedy_re = re.compile('^commit[ \t]+[a-f0-9]{40}$')
    # Bare hash matching pattern
    githash_re = re.compile('[a-f0-9]{40}', re.IGNORECASE)

    for line in message_lines:
        gitrefs = gitlog_greedy_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
    # We return empty arrays if no commit IDs are found
    gitref_list = set(gitref_list)
    gitref_list = list(gitref_list)
    gitref_list.sort()
    return gitref_list


def extract_ucid(message, parent_ids):
    """Extract upstream commit ID from the message."""
    message_lines = message.split('\n')
    gitref_list = []
    # Pattern for 'git show <commit>' (and git log) based backports
    gitlog_re = re.compile('^commit [a-f0-9]{40}$')
    # Pattern for 'git cherry-pick -x <commit>' based backports
    gitcherrypick_re = re.compile(r'^\(cherry picked from commit [a-f0-9]{40}\)$')
    # Bare hash matching pattern
    githash_re = re.compile('[a-f0-9]{40}', re.IGNORECASE)
    # Look for 'RHEL-only' patches
    rhelonly_re = re.compile('^Upstream( Status)?:.*RHEL.*only', re.IGNORECASE)
    # Look for 'Posted' patches, posted upstream but not in a git tree yet
    posted_re = re.compile('^Upstream( Status)?:.*Posted', re.IGNORECASE)

    # Handle merge commits
    if len(parent_ids) > 1:
        gitref_list.append("Merge")

    for line in message_lines:
        gitrefs = gitlog_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        gitrefs = gitcherrypick_re.findall(line)
        for ref in gitrefs:
            for githash in githash_re.findall(ref):
                if githash not in gitref_list:
                    gitref_list.append(githash)
        if rhelonly_re.findall(line):
            gitref_list.append("RHELonly")
        if posted_re.findall(line):
            gitref_list.append("Posted")
    # We return empty arrays if no commit IDs are found
    gitref_list = set(gitref_list)
    gitref_list = list(gitref_list)
    gitref_list.sort()
    return gitref_list


# Just a generic convoluted way of taking a line and a diff_re and searching
# I forgot why the difference between group0 and group1.  I suck at python
# string searching.
# This function handled 4 other pre-compiled searches for changelogs, subject
# lines, and other stuff, so it evolved to something complicated.
def rhkl_search_compiled(line, re_list, group0=False, first=True):
    m = []
    for l in re_list:
        match = l.search(line)
        if match:
            if group0 and match.group(0):
                # print "Found: %s" % match.group(0)
                m.append(match.group(0))
            elif not group0 and match.group(1):
                # print "Found1: %s" % match.group(1)
                m.append(match.group(1))
            if first:
                break

    return m


def filter_diff(diff):
    # this works by going through each patchlet and filtering out as
    # much junk as possible.  If anything is left, consider it a hit and
    # save the _whole_ patchlet for later.  Proceed to next patchlet.

    in_header = True
    hit = False
    header = []
    new_diff = []
    cache = []
    diff_compiled = []

    # sort through rules to see if this chunk is worth saving
    # most of the filters are patch header junk.  Don't care if those
    # offsets are different.
    # The last filter is the most interesting: filter context differences
    # Basically that just filters out noise that changed around
    # the patch and not the parts of the patch that does anything.
    # IOW, the lines in a patch with _no_ ± in front.
    # Personal preference if that is interesting or not.

    diff_re= ["^[+|-]index ",
              "^[+|-]--- ",
              r"^[+|-]\+\+\+",
              "^[+|-]diff ",
              "^[+|-]$",
              "^[+|-]@@ ",
              "^[+|-]new file mode ",
              "^[+|-]deleted file mode ",
              "^[+|-] " ]

    for d in diff_re:
        diff_compiled.append(re.compile(d))

    # end pre-compile stuff

    for d in diff:

        # save the header stuff in case there is stuff to print
        if in_header:
            if d[0:2] == "@@":
                in_header = False
            else:
                header.append(d)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if d[0:2] == "@@":
            if hit:
                #only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [ d ]
            continue

        # auto save line
        cache.append(d)

        # filter lines
        if d[0] == " ":
            # patch context, ignore
            continue
        if rhkl_search_compiled(d, diff_compiled, group0=True):
            # hit junk, skip
            continue

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    return new_diff


def get_partial_diff(diff, filelist):
    """Extract partial diff, used for partial backport comparisons."""
    # this is essentially a reimplementation of the filterdiff binary in python

    in_wanted_file = False
    in_header = True
    hit = False
    partial_diff = ""
    header = []
    new_diff = []
    cache = []

    for line in diff.split('\n'):
        if line.startswith("diff --git "):
            in_wanted_file = False
            for path in filelist:
                if path in line:
                    in_wanted_file = True
                    continue

        if not in_wanted_file:
            continue

        # save the header stuff in case there is stuff to print
        if in_header:
            if line[0:2] == "@@":
                in_header = False
            else:
                header.append(line)
                continue

        # hit boundary, save cached patchlet, if it wasn't filtered
        if line[0:2] == "@@":
            if hit:
                # only print header if there is a hit
                new_diff.extend(header)
                new_diff.extend(cache)
                hit = False
                header = []

            # reset cache and start on new patchlet
            cache = [line]
            continue

        # auto save line
        cache.append(line)

        # nothing got flagged, must be important
        hit = True

    # flush final piece
    if hit:
        new_diff.extend(header)
        new_diff.extend(cache)

    for line in new_diff:
        partial_diff += "%s\n" % line

    return partial_diff


def get_submitted_diff(commit):
    """Extract diff for submitted patch."""
    diff = ""
    filelist = []

    for path in commit:
        diff += "diff --git a/" + path['old_path'] + " "
        diff += "b/" + path['new_path'] + "\n"
        diff += "index blahblah..blahblah 100644\n"
        diff += "--- a/" + path['old_path'] + "\n"
        diff += "+++ b/" + path['new_path'] + "\n"
        diff += path['diff']
        if path['old_path'] not in filelist:
            filelist.append(path['old_path'])
        if path['new_path'] not in filelist:
            filelist.append(path['new_path'])

    return (diff, filelist)


def raise_kabi_red_flag(message, diff):
    if "kabi" in message.lower():
        return True
    if "genksyms" in message.lower():
        return True
    if "rh_reserved" in message.lower():
        return True

    if "kabi" in diff.lower():
        return True
    if "genksyms" in diff.lower():
        return True
    if "rh_reserved" in diff.lower():
        return True

    return False


def mr_get_diff_ids(mrequest):
    """Get the sorted list of diff ids in the MR."""
    diffs = mrequest.diffs.list()
    diff_ids = []
    for diff in diffs:
        diff_ids.append(diff.id)
    diff_ids.sort()
    return diff_ids


def mr_get_last_two_diff_ranges(mrequest, diff_ids):
    """Get the head and start sha plus created_at for the latest two diffs in MR."""
    old = {'head': '', 'start': '', 'created': ''}
    latest = {'head': '', 'start': '', 'created': ''}
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        old['head'] = latest['head']
        old['start'] = latest['start']
        old['created'] = latest['created']
        latest['head'] = diff.head_commit_sha
        latest['start'] = diff.start_commit_sha
        latest['created'] = diff.created_at

    return (latest, old)


def get_diffs_from_mr(mrequest, diff_ids, latest, old):
    """Extract the current and prior MR diffs from the MR."""
    old_diff = None
    new_diff = None
    for diff_id in diff_ids:
        diff = mrequest.diffs.get(diff_id)
        if diff.head_commit_sha == latest['head']:
            new_diff = get_submitted_diff(diff.diffs)
        elif diff.head_commit_sha == old['head']:
            old_diff = get_submitted_diff(diff.diffs)

    if new_diff is not None and old_diff is not None:
        return (old_diff[0], new_diff[0])

    return (None, None)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
