How to use this tool:


Prerequisites:
--------------
$ sudo dnf install python3-dialog python3-gitlab python3-GitPython python3-gitdb

The above are all available for Fedora, but some may not be present for RHEL. They can probably be built from Fedora source rpms though, and/or found in EPEL.

A gitlab account and token set up with API access, and a properly configured ~/.python-gitlab.cfg are also required. Example:

[global]
default = gitlab.com
ssl_verify = true
timeout = 5

[gitlab.com]
url = https://gitlab.com
private_token = <token>
api_version = 4

The utility actually looks specifically for the stanza matching the global default setting in the config to load up, so be sure it's properly set, or connectivity won't work. Set up your token at https://gitlab.com/-/profile/personal_access_tokens and paste the string into your cfg.


Installing:
-----------
Drop the python files somewhere in your PATH -- personally, I just create a symlink /usr/local/bin/rhpatchreview to my git tree's rhpatchreview.py and run it that way.


Use:
----
This tool can be run directly, as well as be wired up as a mutt hook, to aid in
quick review of patches submitted to rhkl. For example:

# Add this to ~/.muttrc or a muttrc include file
# esc p to run patch through rhpatchreview
macro index,pager \ep "<pipe-message>cat - > ~/temp/mutt-buffer.tmp\n \
<shell-escape> /usr/local/bin/review-mr.sh ~/temp/mutt-buffer.tmp\n"

# Direct execution:
rhpatchreview -m <merge request URL>

# Project-listing-based execution:
rhpatchreview -p <project>
(Supported projects: rhel-9, rhel-8, rhel-7, rhel-alt-7, rhel-6, kernel-test)

$ rhpatchreview.py -h
Usage: rhpatchreview [-g <gitrepo>] [-a] [-f <file>] [-g <gitrepo>] [-q] [-n] ([-m] <gitlab MR url> | [-p] <project>)

Options:
  --version             show program's version number and exit
  -h, --help            show this help message and exit
  -a, --autoskip        automatically skip over showing info for patches that
                        match upstream commits perfectly
  -f FILTERDIFF_EXCLUDES, --filter=FILTERDIFF_EXCLUDES
			filterdiff exclude file, listing files to exclude from
			upstream diffs
  -g GITREPO, --gitrepo=GITREPO
			check discovered git hashes against specified upstream
			git tree
  -m MR, --merge-request=MR
			Merge Request URL
  -n, --no-feedback     disable UI to leave feedback on the MR
  -p PROJECT, --project=PROJECT
                        Show all MRs for project
  -q, --quiet           be quiet


Some of these options can now be set via you ~/.gitconfig file as well.
For example:

[rhpatchreview]
        gitrepo = /src/linux
        autoskip = True
	nofeedback = True
	filterdiffexcludes = ~/.rhpatchreview-excludes

Command line flags will always override config file settings.
