#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# rhpatchreview - Red Hat Patch Reviewing Tool
#
# This tool is intended for RHEL kernel patch review, complete with
# hooks into git and gitlab.
#
# Copyright (C) 2012-2021 Red Hat Inc.
# Author: Jarod Wilson <jarod@redhat.com>
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.  See http://www.gnu.org/copyleft/gpl.html for
# the full text of the license.

import os
import sys
import re
import optparse
import tempfile
from subprocess import Popen, PIPE
# python3-dialog
import dialog
# python3-GitPython
import git
# python3-gitdb
from gitdb.exc import BadName, BadObject
# python3-gitlab
import gitlab
from pathlib import Path

# our core library, shared with rhcheckpatch
from rhpatchparser import Match as m
import rhpatchparser as pp

info = ""
notes = ""

def add_info(str):
    global info
    info = info + str


def get_dialog_height_width():
    p = Popen(["tput", "lines"], stdout=PIPE)
    h = int(p.communicate()[0]) - 8
    p = Popen(["tput", "cols"], stdout=PIPE)
    w = int(p.communicate()[0]) - 8
    return (h, w)


def get_diffstat(diff):
    diff_f = tempfile.NamedTemporaryFile(suffix=".diff", mode="w")
    diffstat_f = tempfile.NamedTemporaryFile(mode="r+")
    diff_f.write("%s\n" % diff)
    diff_f.flush()

    os.system("diffstat -p1 %s > %s" % (diff_f.name, diffstat_f.name))
    diffstat_f.flush()
    diffstat = "".join(diffstat_f.readlines())
    diff_f.close()
    diffstat_f.close()

    return diffstat


def show_vimdiff(opts, commit_id, submission):
    fu = tempfile.NamedTemporaryFile(mode="w")
    fs = tempfile.NamedTemporaryFile(mode="w")
    g = git.Git(opts.gitrepo)
    print(commit_id)
    upstream_diff = g.diff("%s^" % commit_id, commit_id, '--no-renames')

    fu.write("Upstream commit %s\n" % commit_id)
    fu.write("%s\n" % upstream_diff)
    fu.flush()
    fs.write("RHEL patch submission\n")
    fs.write("%s" % submission)
    fs.flush()
    os.system("vimdiff %s %s" % (fu.name, fs.name))
    fu.close()
    fs.close()


def show_diff(mr_desc, message, diff):
    diffstat = get_diffstat(diff)
    f = tempfile.NamedTemporaryFile(suffix=".patch", mode="w")
    f.write("%s\n---[End MR Desc]---\n" % mr_desc)
    f.write("%s\n" % message)
    f.write("%s\n--- \n" % diffstat)
    f.write("%s\n" % diff)
    f.flush()
    os.system("vim %s" % f.name)
    f.close()


def show_info_and_actions(opts, d, mreq, commit, gitref, match, diff_diffs):
    global notes
    d.add_persistent_args(["--backtitle", "Patch Review Console"])
    d.add_persistent_args(["--title", "Patch Metadata for MR%s" % opts.mr.split("/")[-1]])
    d.add_persistent_args(["--ok-label", "Select Task"])
    d.add_persistent_args(["--cancel-label", "Exit"])
    choices = []

    choices.append(["Done", "Done with this patch, go to next, if any"])
    diff = commit.diff()

    if match not in (m.NOUCID, m.RHELONLY, m.POSTED, m.MERGECOMMIT):
        choices.append(["Vimdiff", "See vimdiff(s) between upstream commit(s) and submission"])

    if match == m.KABI:
        choices.append(["Peek-KABI-Awooga", "Peek at the patch and scrutinize kABI issues"])
    else:
        choices.append(["Peek", "Peek at the patch in vim"])

    if match in (m.NOUCID, m.POSTED):
        choices.append(["Override", "See vimdiff between override hash and submission"])

    if diff_diffs is not None and len(diff_diffs) > 0:
        choices.append(["Set-diff", "See interdiff with prior set version"])

    choices.append(["Note", "Add review note for this patch"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    diffstr = pp.get_submitted_diff(diff)
    while 1:
        (code, val) = d.menu(text=info, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Done":
                return match
            elif val == "Vimdiff":
                show_vimdiff(opts, gitref, diffstr[0])
            elif val == "Override":
                (icode, override) = d.inputbox("Hash to compare with patch \'%s\'" % commit.title, init="", height=h, width=w)
                if icode == d.DIALOG_OK and override:
                    show_vimdiff(opts, override, diffstr[0])
            elif val in ("Peek-KABI-Awooga", "Peek"):
                show_diff(mreq.description, commit.message, diffstr[0])
            elif val == "Set-diff":
                diff_diffs_str = ""
                for diff_bit in diff_diffs:
                    diff_diffs_str += f'{diff_bit}\n'
                revs = len(mreq.diffs.list())
                show_diff(mreq.description, f"v{revs - 1} to v{revs} interdiff\n", diff_diffs_str)
            elif val == "Note":
                notes += " - "
                (icode, note) = d.inputbox("Review note for patch \'%s\'" % commit.title, init="", height=h, width=w)
                notes += note + "\n"
            else:
                return m.BADMAIL
        else:
            return m.BADMAIL

    return match


def format_commit_string(commit, pnum, ptotal):
    info = ""
    info = "[%s of %s] %s (\"%s\")" % (pnum, ptotal, commit.short_id, commit.title)
    return info


def compare_with_detected_gitref(commit, gitrefs, pnum, ptotal, opts):
    global notes
    commit_found = False
    commit_info = format_commit_string(commit, pnum, ptotal)

    csi = commit.short_id
    for gitref in gitrefs:
        if gitref == "RHELonly":
            add_info("Upstream: %s is a RHEL-only patch\n" % csi)
            notes += "RHEL-only: %s\n" % commit_info
            return (m.RHELONLY, gitref)
        elif gitref == "Posted":
            add_info("Upstream: %s is Posted\n" % csi)
            notes += "Posted: %s\n" % commit_info
            return (m.POSTED, gitref)
        elif gitref == "Merge":
            add_info("Upstream: %s is a merge commit\n" % csi)
            notes += "Merge: %s\n" % commit_info
            return (m.MERGECOMMIT, gitref)

    repo = git.Repo(opts.gitrepo)

    for gitref in gitrefs:
        try:
            ucommit = repo.commit(gitref)
            commit_found = True
            break
        except (BadObject, BadName, ValueError):
            add_info("NoUCID: commit %s not found in %s\n" % (gitref, opts.gitrepo))
            notes += "NoUCID: %s\n" % commit_info

    if not commit_found:
        return (m.NOUCID, None)

    udiff = repo.git.diff("%s^" % ucommit, ucommit, '--no-renames')
    diff = pp.get_submitted_diff(commit.diff())
    interdiff = pp.compare_commits(udiff, diff[0])
    if len(interdiff) == 0:
        add_info("Full: Submission %s matches upstream commit %s perfectly\n" % (csi, gitref))
        if opts.autoskip:
            sys.stdout.write("* Full: %s\n" % commit_info)
            sys.stdout.write("  matches upstream commit %s perfectly\n" % gitref)
        notes += "Full: %s\n" % commit_info
        return (m.FULL, gitref)

    mydiff = pp.get_submitted_diff(commit.diff())
    udiff_part = pp.get_partial_diff(udiff, mydiff[1])
    idiff_part = pp.compare_commits(udiff_part, mydiff[0])
    if len(idiff_part) == 0:
        add_info("Partial: Submission %s matches partial upstream commit %s perfectly\n" % (csi, gitref))
        if opts.autoskip:
            sys.stdout.write("* Partial: %s\n" % commit_info)
            sys.stdout.write("  matches partial upstream commit %s perfectly\n" % gitref)
        notes += "Partial: %s\n" % commit_info
        return (m.PARTIAL, gitref)

    if opts.filterdiff_excludes:
        interdiff = pp.compare_commit_to_upstream_filtered(udiff, diff[0], opts.filterdiff_excludes)
        if len(interdiff) == 0:
            add_info("Partial: Submission %s matches upstream commit %s after filtering\n" % (csi, gitref))
            if opts.autoskip:
                sys.stdout.write("* Partial: %s\n" % commit_info)
                sys.stdout.write("  matches upstream commit %s perfectly after filtering\n" % gitref)
            notes += "Filtered: %s\n" % commit_info
            return (m.PARTIAL, gitref)

    if pp.raise_kabi_red_flag(commit.message, diff[0]):
        add_info("KABI: Submission %s not identical to upstream commit %s\n" % (csi, gitref))
        notes += "KABI: %s\n" % commit_info
        return (m.KABI, gitref)

    add_info("Diffs: Submission %s not identical to upstream commit %s\n" % (csi, gitref))
    notes += "Diffs: %s\n" % commit_info

    return (m.DIFFS, gitref)


def review_commit(gl_proj, d, mreq, commit, pnum, ptotal, diff_diffs, opts):
    global info
    info = ""
    global notes
    perfecto = False
    compare = True
    match = m.NOUCID
    gitref = None
    bad_format_gitref = False

    message = commit.message

    if not opts.quiet:
        approvals = mreq.approvals.get()
        add_info(f"Title: [{gl_proj.name}/{mreq.target_branch} ({pnum}/{ptotal})] {commit.title}\n")
        add_info(f"Author: {commit.author_name} <{commit.author_email}>\n")
        add_info(f"Labels:")
        for label in mreq.labels:
            if not label.startswith("Subsystem:"):
                add_info(f" {label},")
        add_info(f"\n")
        add_info(f"Approved-by:")
        for approval in approvals.approved_by:
            add_info(f" {approval['user']['name']},")
        add_info(f"\n")

    ref = gl_proj.commits.get(commit.id)
    gitrefs = pp.extract_ucid(message, ref.parent_ids)
    if len(gitrefs) == 0:
        gitrefs = pp.extract_ucid_greedy(message)
        bad_format_gitref = True

    if len(gitrefs) > 1:
        notes += "WARNING: %s\n" % format_commit_string(commit, pnum, ptotal)
        notes += " - multiple git references found in this submission!\n"
    elif len(gitrefs) == 0:
        notes += "NoUCID: %s\n" % format_commit_string(commit, pnum, ptotal)
        notes += " - no upstream references found in this submission!\n"
        compare = False

    if opts.gitrepo:
        if compare:
            (match, gitref) = compare_with_detected_gitref(commit, gitrefs, pnum, ptotal, opts)
            if bad_format_gitref == True:
                notes += " - gitref line format incorrect, will fail webhook CommitRefs check"
    else:
        notes += "N/A: %s\n" % commit.title
        for gitref in gitrefs:
            if gitref == "RHELonly":
                add_info("Upstream: RHEL-only patch\n")
                match = m.RHELONLY
                break
            elif gitref is not None:
                add_info("Upstream: %s\n" % gitref.strip())

    if not re.search("redhat.com", commit.author_email):
        notes += " - patch not from a Red Hat email address\n"

    found_sig = False
    for line in message.split("\n"):
        if line.startswith("Signed-off-by:"):
            if "redhat.com" in line:
                found_sig = True

    if not found_sig:
        notes += " - no valid redhat.com DCO/Signed-off-by: found\n"

    perfecto = (match == m.FULL or match == m.PARTIAL)
    if perfecto and opts.autoskip:
        return match

    bzurlpfx="https://bugzilla.redhat.com/show_bug.cgi?id="
    bugs = pp.extract_bzs(message)

    if len(bugs) > 0:
        for bug in bugs:
            add_info("Bugzilla: ")
            if not opts.quiet:
                add_info("%s" % bzurlpfx)
            add_info("%s\n" % bug)
    elif match != m.MERGECOMMIT:
        notes += " - no bugzilla link found in submission\n"

    cves = pp.find_cve_numbers(commit.title, message)
    for cve in cves:
        add_info("CVE ref: %s%s\n" % (bzurlpfx, cve))

    match = show_info_and_actions(opts, d, mreq, commit, gitref, match, diff_diffs)

    return match


def review_notes(notes):
    n = tempfile.NamedTemporaryFile(suffix=".txt", mode="r+")
    n.write("%s\n" % notes)
    n.flush()
    os.system("vim %s" % n.name)

    return n


def leave_mr_feedback(gl_proj, d, mr_id, mreq, header, opts, comment_file):
    d.add_persistent_args(["--backtitle", "Leave Merge Request Feedback"])
    d.add_persistent_args(["--title", "Details for MR%s" % mr_id])
    d.add_persistent_args(["--ok-label", "Choose"])
    d.add_persistent_args(["--cancel-label", "Quit"])
    choices = []

    # my_ack = f"Acked-by: {opts.name} <{opts.email}>"
    my_nack = f"Nacked-by: {opts.name} <{opts.email}>"
    mr_is_mine = (opts.name == mreq.author['name'])

    if mr_is_mine:
        header += "\nThis looks like a merge request you submitted.\n"
    else:
        header += f"\nYou can provide feedback on {gl_proj.name} MR{mr_id}.\n\n"

    already_acked = False
    approvals = mreq.approvals.get()
    for approval in approvals.approved_by:
        if opts.name in approval['user']['name']:
            header += "You have Approved this Merge Request.\n"
            already_acked = True

    choices.append(["Quit", "Quit out of Merge Request review console"])

    if not mr_is_mine:
        if not already_acked:
            choices.append(["Approve", "Approve this Merge Request"])
        else:
            choices.append(["Unapprove", "Unapprove this Merge Request"])

    choices.append(["Nack", "Nack this Merge Request"])
    choices.append(["Comment", "Comment on this Merge Request"])
    choices.append(["View", "View existing comments on this Merge Request"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    while 1:
        (code, val) = d.menu(text=header, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Quit":
                return
            elif val == "Approve":
                approval = mreq.approve()
                header += "You have Approved this Merge Request.\n"
            elif val == "Unapprove":
                approval = mreq.unapprove()
                header += "You have Unapproved this Merge Request.\n"
            elif val == "Nack":
                if already_acked:
                    approval = mreq.unapprove()
                discussion = mreq.discussions.create({'body': f'{my_nack}'})
            elif val == "Comment":
                os.system("vim %s" % comment_file.name)
                comment_file.flush()
                comment_file.seek(0)
                comment = comment_file.read()
                comment = Path(comment_file.name).read_text()
                discussion = mreq.discussions.create({'body': f'{comment}'})
            elif val == "View":
                notes = ""
                discussions = mreq.discussions.list()
                for disc in discussions:
                    discussion = mreq.discussions.get(disc.id)
                    for note in discussion.attributes['notes']:
                        notes += f"\n----[From: {note['author']['name']}]----\n"
                        notes += note['body']
                        notes += "\n"
                note_file = review_notes(notes)
                note_file.close()
            else:
                return
        else:
            return


def read_options_from_gitconfig(opts):
    gitconfig = git.GitConfigParser([os.path.normpath(os.path.expanduser("~/.gitconfig"))], read_only=True)
    if not opts.name:
        try:
            opts.name = gitconfig.get_value('user', 'name')
        except:
            opts.name = ""
    if not opts.email:
        try:
            opts.email = gitconfig.get_value('user', 'email')
        except:
            opts.email = ""
    if not opts.gitrepo:
        try:
            opts.gitrepo = gitconfig.get_value('rhpatchreview', 'gitrepo')
        except:
            opts.gitrepo = ""
    if not opts.autoskip:
        try:
            opts.autoskip = gitconfig.get_value('rhpatchreview', 'autoskip')
        except:
            opts.autoskip = False
    if not opts.no_feedback:
        try:
            opts.no_feedback = gitconfig.get_value('rhpatchreview', 'nofeedback')
        except:
            opts.no_feedback = False
    if not opts.filterdiff_excludes:
        try:
            opts.filterdiff_excludes = gitconfig.get_value('rhpatchreview', 'filterdiffexcludes')
        except:
            opts.filterdiff_excludes = ""

    return opts


def process_merge_request(gl, opts, d, mr_baseurl):
    global notes

    mr_id = opts.mr.split("/")[-1]
    proj_parts = opts.mr.strip("https://gitlab.com/").split("/")[0:5]
    proj_with_namespace = "/".join(proj_parts)
    gl_proj = gl.projects.get(proj_with_namespace)
    mreq = gl_proj.mergerequests.get(mr_id)
    approvals = mreq.approvals.get()
    mr_has_deps = False
    merge_commits = 0

    notes += f"Submitter: {mreq.author['name']}\n"
    notes += f"MR-URL: {opts.mr}\n"
    notes += f'Title: [{gl_proj.name}/{mreq.target_branch}] {mreq.title}\n'
    review_header = notes + '\n'
    notes += f'{mreq.labels}\n'
    if len(approvals.approved_by) > 0:
        notes += f'Approved-by: '
        review_header += f'Approved-by: '
        for approval in approvals.approved_by:
            notes += f"{approval['user']['name']}, "
            review_header += f"{approval['user']['name']}, "
        notes += f'\n\n'
        review_header += f'\n\n'
    notes += f'Description:\n------------\n{mreq.description}\n------------\n\n'

    series_start = mreq.diff_refs['start_sha']
    for label in mreq.labels:
        if label.startswith("Dependencies::"):
            dep_scope = label.split("::")[-1]
            if dep_scope != "OK":
                mr_has_deps = True
                series_start = dep_scope

    diff_ids = pp.mr_get_diff_ids(mreq)
    diff_diffs = None
    number_of_revs = len(diff_ids)
    if number_of_revs >= 2:
        notes += f'Patchset has {number_of_revs} revisions, '
        review_header += f'* Patchset has {number_of_revs} revisions, '
        (latest, old) = pp.mr_get_last_two_diff_ranges(mreq, diff_ids)
        (old_diff, new_diff) = pp.get_diffs_from_mr(mreq, diff_ids, latest, old)
        if old_diff is not None and new_diff is not None:
            diff_diffs = pp.compare_commits(old_diff, new_diff)

        if diff_diffs is not None and len(diff_diffs) > 0:
            notes += f'and there are '
            review_header += f'and there are '
        else:
            notes += f'but there are no '
            review_header += f'but there are no '
        notes += f'differences between this version and the prior one.\n\n'
        review_header += f'differences between this version and the prior one.\n'

    last_diff = None
    for diff_id in diff_ids:
        last_diff = diff_id

    diff_data = mreq.diffs.get(last_diff)
    full_diff = pp.get_submitted_diff(diff_data.diffs)
    diffstat = get_diffstat(full_diff[0])
    notes += f'MR diffstat (includes any dependencies):\n'
    notes += f'{diffstat}\n'

    commits = mreq.commits()
    i = 0
    dep_count = 0
    pcount = len(commits)
    commit_list = []
    for commit in commits:
        # Skip merge commits
        if mr_has_deps:
            ref = gl_proj.commits.get(commit.id)
            if len(ref.parent_ids) > 1:
                merge_commits += 1
                continue
        commit_list.append(commit)
        if mr_has_deps and commit.id.startswith(series_start):
            dep_count = pcount - i
            pcount = i
        i += 1
    dep_count -= merge_commits
    notes += f'Number of commits in MR: {pcount}\n\n'
    review_header += f'* Number of commits in MR: {pcount}\n'
    if mr_has_deps:
        review_header += f'* Number of merge commits in MR: {merge_commits}\n'
        review_header += f'* Number of dep commits in MR: {dep_count}\n'
    notes += f'Per-patch evaluations:\n'
    notes += f'----------------------\n'

    i = 0
    hit_dep_sha = False
    all_match = True
    for commit in reversed(commit_list):
        if mr_has_deps and not hit_dep_sha:
            if commit.id.startswith(series_start):
                hit_dep_sha = True
            continue
        i += 1
        match = review_commit(gl_proj, d, mreq, commit, i, pcount, diff_diffs, opts)
        # sys.stdout.write("Got %s for %s\n" % (match, commit.short_id))
        if match != m.FULL and match != m.PARTIAL:
            all_match = False
        # misappropriating BADMAIL as grounds to exit the loop, not used elsewhere
        if match == m.BADMAIL:
            return match

    # view compiled patch review notes for set
    if len(notes) > 0:
        note_file = review_notes(notes)

    # leave MR review feedback, if this functionality is enabled
    if all_match:
        review_header += '* All commits in MR match upstream\n'
    else:
        review_header += '* This set contains variances from upstream\n'
    if not opts.no_feedback:
        leave_mr_feedback(gl_proj, d, mr_id, mreq, review_header, opts, note_file)
    if len(notes) > 0:
        note_file.close()

    _ = os.system("clear")
    return 0


def get_mr_from_project_index(gl, d, mr_baseurl, opts):
    proj_parts = mr_baseurl.split('/')[3:7]
    proj_with_namespace = "/".join(proj_parts)
    proj_with_namespace += f'/{opts.project}'
    gl_proj = gl.projects.get(proj_with_namespace)
    mr_list = gl_proj.mergerequests.list()

    d.add_persistent_args(["--backtitle", "Merge Request Listing for %s" % opts.project])
    d.add_persistent_args(["--title", "Pick an MR..."])
    d.add_persistent_args(["--ok-label", "Choose"])
    d.add_persistent_args(["--cancel-label", "Quit"])
    choices = []

    choices.append(["Quit", "Quit out of Merge Request review console"])

    header = f"List of Merge Requests for {opts.project}, not yet approved by you\n"

    for mreq in mr_list:
        already_acked = False
        approvals = mreq.approvals.get()
        for approval in approvals.approved_by:
            if opts.name in approval['user']['name']:
                already_acked = True
        if already_acked:
            choices.append([f'{mreq.iid}', f"[ACKED] {mreq.title} // from {mreq.author['name']}"])
        else:
            choices.append([f'{mreq.iid}', f"{mreq.title} // from {mreq.author['name']}"])

    mh = len(choices)
    (h, w) = get_dialog_height_width()

    while 1:
        (code, val) = d.menu(text=header, height=h, width=w, menu_height=mh, choices=choices)
        if code == d.DIALOG_OK:
            if val == "Quit":
                return val
            else:
                opts.mr = mr_baseurl + f'{opts.project}/-/merge_request/{val}'
                process_merge_request(gl, opts, d, mr_baseurl)
        else:
            return "Quit"

    return None


def main(argv):
    parser = optparse.OptionParser(usage='%prog [-g <gitrepo>] [-a] [-f <file>] [-g <gitrepo>] [-q] [-n] ([-m] <gitlab MR url> | [-p] <project>)', version="%prog 0.2")
    parser.add_option('-a', '--autoskip', action='store_true', dest='autoskip', help='automatically skip over showing info for patches that match upstream commits perfectly')
    parser.add_option('-f', '--filter', action='store', dest='filterdiff_excludes', help='filterdiff exclude file, listing files to exclude from upstream diffs')
    parser.add_option('-g', '--gitrepo', action='store', dest='gitrepo', help='check discovered git hashes against specified upstream git tree')
    parser.add_option('-m', '--merge-request', action='store', dest='mr', help='Merge Request URL')
    parser.add_option('-n', '--no-feedback', action='store_true', dest='no_feedback', help='disable UI to leave feedback on the MR')
    parser.add_option('-p', '--project', action='store', dest='project', help='Show all MRs for project')
    parser.add_option('-N', '--name', action='store', dest='name', help='Set your username (for acks/nacks)')
    parser.add_option('-e', '--email', action='store', dest='email', help='Set your email (for acks/nacks)')
    parser.add_option('-q', '--quiet', action='store_true', dest='quiet', help='be quiet')

    (opts, args) = parser.parse_args()

    mr_baseurl = "https://gitlab.com/redhat/rhel/src/kernel/"
    mr_action = "merge_request"
    projects = ["rhel-9", "rhel-8", "rhel-7", "rhel-alt-7", "rhel-6", "kernel-test"]

    opts = read_options_from_gitconfig(opts)

    try:
        gl = gitlab.Gitlab.from_config()
    except Exception:
        parser.error("Please set up ~/.python-gitlab.cfg!\n" \
                     " => See https://python-gitlab.readthedocs.io/en/stable/cli.html")

    d = dialog.Dialog(dialog="dialog")
    if opts.project:
        if opts.project not in projects:
            parser.error("Unknown project (%s) specified. Known projects are %s.\n"
                         % (opts.project, projects))
        while 1:
            opts.mr = get_mr_from_project_index(gl, d, mr_baseurl, opts)
            if opts.mr == "Quit":
                _ = os.system("clear")
                return 0

    # if any args are left, assume it's a gitlab MR url w/o -m specified
    if not opts.mr and args:
        if len(args) > 1:
            parser.error("Too many args. We need a single gitlab MR url.")

        if mr_baseurl in args[0] and mr_action in args[0]:
            opts.mr = args[0]
        else:
            parser.error("Unknown arg. We need a single gitlab MR url.")

    if not opts.mr:
        parser.error("No gitlab MR url specified.")

    if not mr_baseurl in opts.mr or not mr_action in opts.mr:
        parser.error("Unrecognized gitlab MR url, only %s<project>/-/%s/<MR#> is valid." \
                     % (mr_baseurl, mr_action))

    process_merge_request(gl, opts, d, mr_baseurl)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
